1. Create the following functions:

A. isBigger -> this function accepts two numbers as parameters (num1, num2) and 
returns the bigger number.



function isBigger(num1, num2){
    if(num1>num2){return num1;
    }else{
        return num2;
    }

    }
};



B. iLoveYou -> this function accepts a language string as a parameter
ex: "English", "Italian", "Tagalog"
and will return i love you in the language in the parameter.
Ex: iLoveYou('Spanish') // will return Te Amo.


function iLoveYou(language){
    if (language === "English")
    {
        return "I love you";
    } else if (language=== "Italian"){
        return "Te Amo";

    } else if (language === "Tagalog"){
        return "Mahal Kita";
    }
    }



C. gradeAssigner -> this function accepts a number as a parameter and returns the corresponding
letter grades based on the following:

function gradeAssigner(grade){if (grade <= 60){
    return "F";
} else if (grade <= 90){
    return "P";
}else if (grade > 90){
    return "S";
}
}


D. pluralizer -> this function accepts two parameters, an animal and a number, and must return
the correct form of the noun.
Ex. pluralizer(dog, 1) must return dog.
Ex. pluralizer(dog, 3) must return dogs.

Challenge:
Include irregular nouns like goose to geese, mouse to mice, etc.